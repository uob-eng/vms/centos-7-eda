packer {
  required_plugins {
    vagrant = {
      source  = "github.com/hashicorp/vagrant"
      version = "~> 1"
    }
  }
}
source "vagrant" "bento-centos-7" {
  communicator = "ssh"
  provider     = "virtualbox"
  source_path  = "bento/centos-7"
}
build {
  name = "centos-7-eda"
  sources = ["source.vagrant.bento-centos-7"]
  provisioner "shell" {
    execute_command = "echo 'vagrant' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    script          = "scripts/provision.sh"
  }
}
