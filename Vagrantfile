# CentOS 7 Electronic Design Automation Virtual Machine
# ======================================================================
#
# A VM to run EDA tools such as Cadence.

# Customisation
# ----------------------------------------------------------------------
#
# Change as desired!

# Mount directory
#
# This is the name of the directory within your home directory to mount
# in the VM. If you prefer not to have a top-level directory, give a
# path instead, such as 'My Documents/COMS30026'. Note that you can use
# any directory within your home directory, and set up further
# directories for individual courses or projects - EDA applications
# won't know to write here, it's just what will be mounted for the VM to
# use.
#
# Don't set this to an empty string, '' - GNOME needs to write many
# files in your home directory, and sharing between different versions
# of the operating system isn't great.
#
mount_dir = 'EDA'

# Shouldn't need to edit below this line

# User info - chomp to remove trailing newline
# ----------------------------------------------------------------------

gid = `id --group`.chomp
group = `id --group --name`.chomp
host_name = `hostname --short`.chomp
uid = `id --user`.chomp
user = `id --user --name`.chomp
user_name = `getent passwd "$USER" | cut -d ':' -f 5 | cut -d ',' -f 1`.chomp

# VM description
# ----------------------------------------------------------------------

Vagrant.configure("2") do |config|
  if ARGV[0] == "ssh"
    config.ssh.username = "#{user}"
  end

  config.vm.box = "centos-7-eda"
  config.vm.hostname = "#{host_name}-vm-#{user}"

  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "/eda", "/eda"
  config.vm.synced_folder "#{ENV['HOME']}/#{mount_dir}", "#{ENV['HOME']}/#{mount_dir}", group: gid, owner: uid

  config.vm.provider "virtualbox" do |vb|
    vb.gui    = true
    vb.cpus   = `nproc`.to_i
    vb.memory = 12288  # MB
  end

  config.vm.provision "shell", inline: <<~EndOfShell
    eol=$'\\n'
    echo 'Hiding vagrant user from the login list'
    echo "[User]${eol}SystemAccount=true" > /var/lib/AccountsService/users/vagrant

    echo "Adding group #{group} with ID #{gid}"
    groupadd --gid "#{gid}" "#{group}"
    echo "Adding user #{user_name} (#{user}) with ID #{uid}"
    useradd --no-create-home --no-user-group --comment "#{user_name}" --gid "#{gid}" --groups wheel --uid "#{uid}" "#{user}"

    echo "Setting password #{user} for user #{user}"
    passwd --stdin "#{user}" <<< "#{user}"
    echo "Allowing sudo without password for user #{user}"
    echo "%#{user} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/#{user}

    echo "Copying skeleton files from /etc/skel to /home/#{user}"
    find /etc/skel -mindepth 1 -maxdepth 1 -exec cp --recursive {} "/home/#{user}/" \\;
    echo "Setting ownership #{user}:#{group} on /home/#{user}"
    chown --recursive #{user}:#{group} "/home/#{user}"
  EndOfShell

  config.vm.post_up_message = <<~EndOfMessage
    The VM GUI should now be visible, or you can use "vagrant ssh".

    Use your username, "#{user}", rather than "vagrant".

    To avoid an untrusted VM having access to UoB authentication, your
    account has been set with your username as the password. The VM runs as
    your user account, so no-one else can access it.

    ========================================================================
    ** WARNING **

    If you save work anywhere within this VM except in the mounted directory
    below, when the VM is closed or you use `vagrant destroy` this work will
    be lost.

    Mounted directory: #{ENV['HOME']}/#{mount_dir}
    ========================================================================
  EndOfMessage
end
