CentOS 7 EDA Virtual Machine
========================================================================

How to lose your work!
------------------------------------------------------------------------

> ### **WARNING**
>
> If you save work anywhere within this VM except in the
> mounted directory mentioned under _Configuration_ below, when the VM
> is closed or you use `vagrant destroy` this **work will be lost**.

How to use
------------------------------------------------------------------------

### Installation

Either use Git:

```bash
cd ~/path/to/vm
git clone https://gitlab.com/uob-eng/vms/centos-7-eda.git .
```

or just copy the Vagrantfile to a directory of your choice.

### Configuration

Edit the `mount_dir` line in the `Vagrantfile` if you want to store
projects in a directory other than `~/EDA`, and make sure the directory
exists in your home directory before running the next steps. For
instance, to use ~/Cadence, set `mount_dir='Cadence'` and create the
directory `~/Cadence` (same as `$HOME/Cadence`) **on the host** before
starting the VM

### Running

```bash
vagrant box add --name 'centos-7-eda' /opt/software/VMs/centos-7-eda/centos-7-eda.box
vagrant up
```
Log in with your username, reusing your username as the password, and
follow your course notes on how to run EDA tools.

N.B.: This will need to be done on any lab machine you use, as VMs are
stored in temporary directories, to avoid taking huge amounts of space
in your home directory. Files stored in the mounted directory (either
`~/EDA` or whatever you change it to in the Vagrantfile) are in your
home directory, so will move around machines with you.

Further information on Vagrant can be found at: https://www.vagrantup.com/docs.

### Cleaning up

When you've finished using the VM, open a terminal in the same directory
and run:

```bash
vagrant destroy
```

### Updating

To update the image to a later version:

```bash
(( $(
  stat --printf '%Y' "$VAGRANT_HOME/boxes/centos-7-eda/0/virtualbox/box-disk001.vmdk"
) >= $(
  stat --printf '%Y' '/opt/software/VMs/centos-7-eda/centos-7-eda.box'
) )) || {
  vagrant box remove 'centos-7-eda'
  vagrant box add --name 'centos-7-eda' /opt/software/VMs/centos-7-eda/centos-7-eda.box
  echo "Updated centos-7-eda.box to latest version"
}

# Or, as one line...
(( $(stat --printf '%Y' "$VAGRANT_HOME/boxes/centos-7-eda/0/virtualbox/box-disk001.vmdk") >= $(stat --printf '%Y' '/opt/software/VMs/centos-7-eda/centos-7-eda.box') )) || { vagrant box remove 'centos-7-eda' ; vagrant box add --name 'centos-7-eda' /opt/software/VMs/centos-7-eda/centos-7-eda.box ; echo "Updated centos-7-eda.box to latest version" ; }
```

How to build
------------------------------------------------------------------------

Install Hashicorp Packer (in the Hashicorp repo that is present on all
lab machines, for instance), then use Git to clone this repository as
above.

```bash
packer build centos-7-eda.pkr.hcl
mv output-bento-centos-7/package.box centos-7-eda.box
rm -rf output-bento-centos-7
```

Then upload the box file to `apps.fen.bris.ac.uk:/data/linux_software/vms/centos-7-eda/`.
