#!/bin/bash
# Don't run yum update, as that will cause a kernel update and then the
# VirtualBox additions will need recompiling for the new kernel!
yum -y groupinstall 'GNOME Desktop'
yum -y remove gnome-classic-session
systemctl set-default graphical.target
yum -y install environment-modules
# Cadence requirements from Linux lab software spreadsheet
yum -y install apr-util compat-db47 glibc-devel glibc-devel.i686 ksh libglvnd-glx libglvnd-glx.i686 libpng12 libXcursor-devel libXcursor-devel.i686 libXext-devel libXext-devel.i686 libXft-devel libXft-devel.i686 libXp-devel libXp-devel.i686 libXrandr-devel libXrandr-devel.i686 libXScrnSaver-devel libXScrnSaver-devel.i686 libXt-devel libXt-devel.i686 libXtst-devel libXtst-devel.i686 mesa-libGL-devel mesa-libGLU mesa-libGLU.i686 motif-devel ncurses-devel tcl tcsh tk xorg-x11-fonts-75dpi xorg-x11-fonts-misc

# Clean up
# ======================================================================

# From https://github.com/chef/bento/blob/main/packer_templates/scripts/rhel/cleanup_yum.sh

echo "reduce the grub menu time to 1 second"
sed -i -e 's/^GRUB_TIMEOUT=[0-9]\+$/GRUB_TIMEOUT=1/' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg

# echo "Remove development and kernel source packages"
# yum -y remove gcc cpp gc kernel-devel kernel-headers glibc-devel elfutils-libelf-devel glibc-headers kernel-devel kernel-headers

echo "remove orphaned packages"
yum -y autoremove

echo "Remove previous kernels that preserved for rollbacks"
if ! command -v package-cleanup >/dev/null 2>&1; then
  yum -y install yum-utils
fi

package-cleanup --oldkernels --count=1 -y

# Avoid ~200 meg firmware package we don't need
# this cannot be done in the KS file so we do it here
echo "Removing extra firmware packages"
yum -y remove linux-firmware

echo "clean all package cache information"
yum -y clean all  --enablerepo=\*;

# Clean up network interface persistence
rm -f /etc/udev/rules.d/70-persistent-net.rules;
mkdir -p /etc/udev/rules.d/70-persistent-net.rules;
rm -f /lib/udev/rules.d/75-persistent-net-generator.rules;
rm -rf /dev/.udev/;

for ndev in /etc/sysconfig/network-scripts/ifcfg-*; do
    if [ "$(basename "$ndev")" != "ifcfg-lo" ]; then
        sed -i '/^HWADDR/d' "$ndev";
        sed -i '/^UUID/d' "$ndev";
    fi
done

echo "truncate any logs that have built up during the install"
find /var/log -type f -exec truncate --size=0 {} \;

echo "remove the install log"
rm -f /root/anaconda-ks.cfg /root/original-ks.cfg

echo "remove the contents of /tmp and /var/tmp"
rm -rf /tmp/* /var/tmp/*

echo "Force a new random seed to be generated"
rm -f /var/lib/systemd/random-seed

echo "Wipe netplan machine-id (DUID) so machines get unique ID generated on boot"
truncate -s 0 /etc/machine-id
if test -f /var/lib/dbus/machine-id
then
  truncate -s 0 /var/lib/dbus/machine-id  # if not symlinked to "/etc/machine-id"
fi

echo "Clear the history so our install commands aren't there"
rm -f /root/.wget-hsts
export HISTSIZE=0

# Minimise
# ======================================================================

# From https://github.com/chef/bento/blob/main/packer_templates/scripts/_common/minimize.sh

# Whiteout root
count=$(df --sync -kP / | tail -n1  | awk -F ' ' '{print $4}')
count=$((count - 1))
dd if=/dev/zero of=/tmp/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
rm /tmp/whitespace

# Whiteout /boot
count=$(df --sync -kP /boot | tail -n1 | awk -F ' ' '{print $4}')
count=$((count - 1))
dd if=/dev/zero of=/boot/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
rm /boot/whitespace

set +e
swapuuid="$(/sbin/blkid -o value -l -s UUID -t TYPE=swap)";
case "$?" in
  2|0) ;;
  *) exit 1 ;;
esac
set -e

if [ "x${swapuuid}" != "x" ]; then
  # Whiteout the swap partition to reduce box size
  # Swap is disabled till reboot
  swappart="$(readlink -f /dev/disk/by-uuid/"$swapuuid")"
  /sbin/swapoff "$swappart" || true
  dd if=/dev/zero of="$swappart" bs=1M || echo "dd exit code $? is suppressed"
  /sbin/mkswap -U "$swapuuid" "$swappart"
fi

sync
